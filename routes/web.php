<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PainterController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\ReviewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PageController::class, 'index'])->name('home');

//painting
Route::get('pokraska', [PageController::class, 'painting'])->name('painting');
Route::get('/pokraska/{slug}', [PageController::class, 'painting_category'])
        ->name('painting_category');

//contact
Route::post('/send', [ContactController::class, 'send'])->name('send');
Route::post('/send_partner', [ContactController::class, 'send_partner'])
        ->name('send_partner');

//reviews
Route::post('/review', [ReviewController::class, 'store'])->name('review');

//master
Route::get('/avtomalyar/{id}', [PageController::class, 'painter'])
        ->name('painter');
Route::get('/avtomalyar/{id}/otzyvy', [PageController::class, 'review_painter'])
        ->name('review_painter');

//Blog
Route::get('/blog', [PageController::class, 'blog'])->name('blog')
        ->name('blog');
Route::get('/post/{slug}', [PageController::class, 'post'])
        ->name('post');
Route::post('/comment-store', [CommentController::class, 'store'])
        ->name('comment_store');

//terms
Route::get('/usloviya-ispolzovaniya', [PageController::class, 'terms'])
        ->name('terms');

//sitemap
Route::get('karta-sajta', [SitemapController::class, 'sitemap'])
        ->name('sitemap');
Route::get('sitemap.xml', [SitemapController::class, 'index']);

//Profile
Route::get('/cabinet', [PainterController::class, 'index'])
        ->name('cabinet');
Route::get('/cabinet/advt/create', [PainterController::class, 'create'])
        ->name('cabinet_advt_create');
Route::post('/cabinet/advt/store', [PainterController::class, 'store'])
        ->name('cabinet_advt_store');
Route::post('/getPhone', [PageController::class, 'getPhone'])
        ->name('getPhone');
Route::get('/cabinet/advt/edit/{id}', [PainterController::class, 'edit'])
        ->name('cabinet_advt_edit');
Route::post('/cabinet/advt/update/{id}', [PainterController::class, 'update'])
        ->name('cabinet_advt_update');

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    
    Voyager::routes();
    
});

