<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaintersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('painters', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->boolean('status')->nullable();
            $table->boolean('vip')->nullable();
            $table->string('tariff')->nullable();
            $table->string('ip_adress')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('company')->nullable();
            $table->string('about')->nullable();
            $table->string('site')->nullable();
            $table->string('country')->nullable();
            $table->string('area')->nullable();
            $table->string('city')->nullable();
            $table->string('street')->nullable();
            $table->string('house')->nullable();
            $table->string('image')->nullable();
            $table->integer('phone_view')->unsigned()->nullable();
            $table->integer('bamper_front')->unsigned()->nullable();
            $table->integer('hood')->unsigned()->nullable();
            $table->integer('wing_front')->unsigned()->nullable();
            $table->integer('roof')->unsigned()->nullable();
            $table->integer('door_front')->unsigned()->nullable();
            $table->integer('door_rear')->unsigned()->nullable();
            $table->integer('trunk_lid')->unsigned()->nullable();
            $table->integer('wing_rear')->unsigned()->nullable();
            $table->integer('bumper_rear')->unsigned()->nullable();
            $table->integer('threshold')->unsigned()->nullable();
            $table->integer('mirror')->unsigned()->nullable();
            $table->integer('part_local')->unsigned()->nullable();
            $table->integer('car_full')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('painters');
    }
}
