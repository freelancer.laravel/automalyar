<?php

namespace Database\Factories;

use App\Models\Painter;
use Illuminate\Database\Eloquent\Factories\Factory;

class PainterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Painter::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $region = self::getRegion();
        
        return [
            'user_id' => 9,
            'status' => 1,
            'first_name' => $this->faker->name(),
            'last_name' => 'макет',
            'email' => $this->faker->unique()->email(),
            'phone' => self::getPhone(),
            'company' => $this->faker->unique()->company(),
            'about' => 'Покраска авто,рехтовка авто,полировка, пайка бамперов, антикоррозийная обработка.',
            'country' => 'Украина',
            'area' => $region['area'],
            'city' => $region['city'],
            'street' => self::getStreet($this->faker->numberBetween(0, 13)),
            'house' => $this->faker->numberBetween(1, 499),
            //'image' => $this->faker->imageUrl(400, 400, 'cats', true, 'Faker', true),
            'bamper_front' => self::ceilCoefficient(2000, 4000, 100),
            'hood' => self::ceilCoefficient(3000, 4000, 100),
            'wing_front' => self::ceilCoefficient(2000, 4000, 100),
            'roof' => self::ceilCoefficient(4000, 10000, 100),
            'door_front' => self::ceilCoefficient(2500, 4000, 100),
            'door_rear' => self::ceilCoefficient(2500, 4000, 100),
            'trunk_lid' => self::ceilCoefficient(2000, 4000, 100),
            'wing_rear' => self::ceilCoefficient(2000, 4000, 100),
            'bumper_rear' => self::ceilCoefficient(2000, 4000, 100),
            'threshold' => self::ceilCoefficient(2000, 3000, 100),
            'mirror' => self::ceilCoefficient(600, 2000, 100),
            'part_local' => self::ceilCoefficient(1000, 2500, 100),
            'car_full' => self::ceilCoefficient(20000, 70000, 100),
            
        ];
    }
    
    public function ceilCoefficient($min, $max, $rate = 50)
    {         
        $number = random_int($min, $max);
	// разделим число на коэффициент, а результат округлим в большую сторону. Потом умножим число на округленный коэффициент
	$rest = ceil($number / $rate) * $rate;
        
	return $rest;
    }
    
    public function getRegion() {
        
        $json = file_get_contents('public/regions.json');
        
        $regions = json_decode($json, 1);
        
        $areas = [
            2121, 2123, 2126, 2134, 2147, 2152, 2155, 2160, 
            2164, 2169, 2173, 2180, 2185, 2188, 2193, 2198, 
            2200, 2204, 2206, 2209, 2212, 2216, 2220, 2224,
        ];
        
        $area = random_int(0, 23);
        
        $city = random_int(0, (count($regions[$areas[$area]]))-1);
        
        $result = ['area' => $areas[$area], 'city' => $regions[$areas[$area]][$city]];
        
        return $result;
    }
    public function getStreet($number) {
        
        $streets = [
            'Советская', 'Тенистая', 'Тихая', 'Школьная', 'К. Маркса', 
            'Блюхера', 'Энгельса', 'Чапаева', 'Конева', 'Кирова',
            'Киевская', 'Московская', 'Симферопорльская', 'Ленина',
        ];
        
        return $streets[$number];
    }
    public function getPhone() {
        
        $operators = [
            '096', '063', '068', '097', '099', '098',   
        ];
        
        $operator = random_int(0, 5);
        
        $number = random_int(1111111, 9999999);
         
        $Phonenumber = $operators[$operator] . $number;
        
        return $Phonenumber;
    }
    
}
