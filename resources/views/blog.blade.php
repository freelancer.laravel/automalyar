@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
  <div id="content" class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="cont maincont">
                    @include('partials.components.breadcrumb')
                    <div class="blog blog-full">
                        <div class="blog-cont">
                            <div id="blog-grid">
                                @foreach($posts as $post)
                                    <article class="blog-grid-i">
                                        <div class="blog-i">
                                            <a href="{{url('/post/' .$post->slug)}}" class="blog-img">
                                                <img src="{{ Voyager::image($post->image) }}" alt="">
                                            </a>
                                            <p class="blog-info">
                                                <a>Статья</a>
                                                <time>{{$post->created_at->format('j, F, Y')}}</time>
                                            </p>
                                            <h3><a href="{{url('/post/' .$post->slug)}}">{{$post->title}}</a></h3>
                                            <p>{{$post->excerpt}} <a class="blog-i-more" href="{{url('/post/' .$post->slug)}}">Читать больше</a></p>
                                        </div>
                                    </article>
                                @endforeach
                            </div>
                            {{ $posts->links('vendor.pagination.default') }}
                        </div>
                    </div>
                </div>
            </main><!-- #main -->
        </div>
    </div><!-- #content -->
@endsection
