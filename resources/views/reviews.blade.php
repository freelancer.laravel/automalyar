@extends('layouts.master')
@section('title', $painter->company. ' отзывы')

@section('content')
<div id="content" class="site-content">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <h1 class="maincont-ttl">{{$painter->company}} отзывы</h1>
                <ul class="b-crumbs">
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li><a href="{{route('painting')}}">Покраска авто</a></li>
                    <li>{{$painter->company}} отзывы</li>
                </ul>
                <article>
                    <div class="prod-tabs-wrap">
                        <div class="prod-rating-wrap reviews-rating-wrap">
                            <p data-rating="{{$middleStar}}" class="prod-rating">
                                <i class="rating-ico" title="1"></i>
                                <i class="rating-ico" title="2"></i>
                                <i class="rating-ico" title="3"></i>
                                <i class="rating-ico" title="4"></i>
                                <i class="rating-ico" title="5"></i>
                            </p>
                            <p class="prod-rating-count"><span class="middle-star-num">{{$middleStarNum ?? '0'}}</span> из <span class="quantity">{{$numberReviews}}</span> (отзывов)</p>
                        </div>
                    </div>
                    <div class="prod-tabs-wrap">
                        <ul class="prod-tabs">
                            <li data-prodtab-num="3" id="prod-reviews" class="active">
                                <a data-prodtab="#prod-tab-3" href="#">Отзывы (<span class="quantity">{{$numberReviews}}</span>)</a>
                            </li>
                            <li data-prodtab-num="2" id="prod-props">
                                <a data-prodtab="#prod-tab-2" href="#">О Компании</a>
                            </li>
                            <li class="prod-tabs-addreview">Добавить отзыв</li>
                        </ul>
                        <div class="prod-tab-cont">
                            <p data-prodtab-num="3" class="prod-tab-mob active" data-prodtab="#prod-tab-3">Отзывы ({{$numberReviews}})</p>
                            <div class="prod-tab prod-reviews" id="prod-tab-3">
                                <div id="reviews">
                                    <div id="review_form_wrapper" class="prod-addreview-form">
                                        <div id="review_form">
                                            <div id="respond" class="comment-respond">
                                                <p class="prod-tab-addreview" id="reply-title">Отзыв о компании</p>
                                                <form action="{{route('review')}}" method="post" id="commentform" class="review-form form-validate">
                                                    @csrf
                                                    <div class="comment-form-rating">
                                                        <p class="stars">
                                                            <span>
                                                                1<input type="radio"  class="star-1" name="rating" value="1" required>
                                                                2<input type="radio"  class="star-2" name="rating" value="2">
                                                                3<input type="radio"  class="star-3" name="rating" value="3">
                                                                4<input type="radio"  class="star-4" name="rating" value="4">
                                                                5<input type="radio"  class="star-5" name="rating" value="5">
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <input type="hidden" name="reviewable_type" value="painter">
                                                    <input type="hidden" name="reviewable_id" value="{{$painter->id}}">
                                                    <input type="text" name="name" placeholder="Имя" data-required="text">
                                                    <input type="text" name="email" placeholder="Email" data-required="text" data-required-email="email">
                                                    <textarea name="description" placeholder="Текст отзыва .." data-required="text"></textarea>
                                                    <button class="submit" type="submit"><i class="fa"></i> Отправить</button>
                                                    <p class="form-result">Отзыв добавлен!</p>
                                                    <p class="error-send">Отзыв не добавлен!</p>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <ul id="comments" class="review-list">
                                        @foreach($reviews as $review)
                                            <li class="prod-review" id="{{$review->id}}">
                                                <h3>{{$review->name}}</h3>
                                                <p class="prod-review-rating" title="Оценка {{$review->rating}} из 5">
                                                    @foreach(range(1, $review->rating) as $number)
                                                        <i class="rating-ico"></i>
                                                    @endforeach
                                                    <a class="review-link" href="#{{$review->id}}">
                                                        <time>{{$review->created_at->format('j, F, Y, H : i')}}</time>
                                                    </a>
                                                </p>
                                                <p>{{$review->description}}</p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <p data-prodtab-num="2" class="prod-tab-mob" data-prodtab="#prod-tab-2">О Компании</p>
                            <div class="prod-tab" id="prod-tab-2">
                                <a class="info" href="{{url('/avtomalyar/'. $painter->id)}}">Полная информация</a>
                            </div>
                        </div>
                    </div>

                </article>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

</div><!-- #content -->
@endsection
