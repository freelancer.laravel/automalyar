
<div class="site-header sticky-off">

    <p class="h-logo">
        <a href="{{url('/')}}"><img src="{{ Voyager::image(setting('site.logo')) }}" alt="logo"></a>
    </p><!--
No Space
    --><div class="h-shop">

        <form method="get" action="#" class="h-search" id="h-search">
            <input type="text" placeholder="Поиск...">
            <button type="submit" disabled=""><i class="ion-search"></i></button>
        </form>

        <ul class="h-shop-links">
            <li class="h-search-btn" id="h-search-btn"><i class="ion-search"></i></li>
            <li class="h-shop-icon h-profile"></li>
            <li class="h-shop-icon h-profile">
                <a href="javascript:void(0);" title="Аккаунт">
                    <i class="fa fa-user"></i>
                </a>
                <ul class="h-profile-links">
                    @auth
                        <li><a href="{{url('cabinet')}}">Кабинет</a></li><!-- comment -->
                    @else
                        <li><a href="{{ route('login') }}">Авторизация</a></li>
                        <li><a href="{{ route('register') }}">Регистрация</a></li>
                    @endauth
                </ul>
            </li>
            @auth
                <li class="h-shop-icon h-profile">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" title="Выход">
                        <i class="fa fa-sign-out"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                
            @endauth
            <li class="h-menu-btn" id="h-menu-btn">
                <i class="ion-navicon"></i> Меню
            </li>
        </ul>
    </div><!--
    No Space
    --><div class="mainmenu">

        <nav id="h-menu" class="h-menu">
            <ul>
                <li class="{{url()->current() === url('/') ? 'active': ''}}">
                    <a class="link-menu-top" href="{{url('/')}}"><i class="fa fa-home"></i> Главная</a>
                </li>
                <li class="{{url()->current() === url('pokraska') ? 'active': ''}} {{request()->segment(1) === 'pokraska' ? 'active': ''}}">
                    <a class="link-menu-top" href="{{url('pokraska')}}"><i class="fa fa-paint-brush"></i> Автомаляры Украины</a>
                </li>
                <li class="{{url()->current() === url('blog') ? 'active': ''}}">
                    <a class="link-menu-top" href="{{url('blog')}}"><i class="fa fa-book"></i> Блог</a>
                </li>
            </ul>
        </nav>

    </div><!--
    No Space
    -->
</div>
