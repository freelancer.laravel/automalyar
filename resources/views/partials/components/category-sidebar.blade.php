@if($categories)
    @foreach($categories as $category)
        @if(count($category->children) > 0)
            <li class="category-parent">
                <a href="javascript:void(0);">{{$category->name}}<span class="count"></span> <i class="fa fa-caret-down"></i></a>
                <ul class="section-sb-list category-children">
                    @foreach($category->children as $category_children)
                        <li>
                            <a href="{{url('/services/'. $category_children->slug)}}" data-type="children" class="{{$category_children->slug}}"><span class="section-sb-label"> {{$category_children->name}} <span class="count"></span></span></a>
                        </li>
                    @endforeach                                          
                </ul>
            </li>
        @else
        <li><a href="{{url('/services/'. $category->slug)}}" data-type="parent" class="{{$category->slug}}"><span class="section-sb-label">{{$category->name}} <span class="count"></span></span></a></li>
        @endif                                               
    @endforeach
@endif

