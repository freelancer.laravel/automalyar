@extends('layouts.master')
@section('title', '404')
@section('content')
	<div id="content" class="site-content">
	   <div id="primary" class="content-area width-full">
	      <main id="main" class="site-main">
		 <div class="cont mt20">
		    <h1 class="maincont-ttl">Страница не найдена</h1>
		    <ul class="b-crumbs">
		        <li><a href="{{url('/')}}">Главная</a></li>
		        <li>Страница не найдена</li>
		    </ul>
		 </div>
		 <div class="maincont page-styling page-full">
		    <div class="cont maincont">
		       <div class="pagecont err404">
		          <p class="err404-ttl">404</p>
		          <p>К сожалению запрашиваемая страница недоступна</p>
		          <form method="get" action="{{url('search')}}" class="header-search">
		             <input value="" name="s" type="text" placeholder="Поиск...">
                             <button type="submit" disabled><i class="fa fa-search"></i></button>
		          </form>
		       </div>
		    </div>
		 </div>
		 <!-- .maincont.page-styling.page-full -->
	      </main>
	      <!-- #main -->
	   </div>
	   <!-- #primary -->    
	</div>
	<!-- #content -->
@endsection
