@extends('layouts.master')
@push ('scripts')
<script type="text/javascript">
    var select_area = {{$area}};
    var select_city = {{$city}};
    //alert(select_area)
    
function Get(yourUrl) {
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET", yourUrl, false);
    Httpreq.send(null);
    return Httpreq.responseText;
}
var linkCityJson = '{{url('/')}}/Json/city-ua.json'
var json_object = JSON.parse(Get(linkCityJson));
let dataList = json_object;

function getAreasDefault(json_object) {
    var json = json_object;
    json.areas.forEach(area => {
        var opt = document.createElement('option');
        opt.value = area.id;
        opt.setAttribute("id", area.id);
        opt.innerHTML = area.name;
        billing_area.appendChild(opt);
    });
}

function getAreas(json_object) {
    var json = json_object;
    json.areas.forEach(area => {
        var opt = document.createElement('option');
        opt.value = area.id;
        opt.setAttribute("id", area.id);
        opt.innerHTML = area.name;
        billing_area.appendChild(opt);
    });
    
    selectSaveArea();
}

//выбирает область которая была сохранена
function selectSaveArea() {
    var area = select_area;
    billing_area.value = area;
    billing_area.dispatchEvent(new Event('change'));
}


billing_area.addEventListener('change', () => {
    if (billing_area.options[billing_area.selectedIndex].value != '') {
        billing_city.innerHTML = '';
        dataList.areas.filter(item => item.id == billing_area.options[billing_area.selectedIndex].value)[0].areas.forEach(
            area => {
                var opt = document.createElement('option');
                //выбирает город который был сохранен
                if(select_city){
                    if (area.id == select_city) {
                        opt.selected = true;
                    }
                }

                opt.value = area.id;
                opt.innerHTML = area.name;
                billing_city.appendChild(opt);
            }
        )
    }
});
// Поиск области по id
function findArea(area, json_object) {
    for (var i = 0; i <= json_object.areas.length - 1; i = i + 1) {
        if (json_object.areas[i].id == area) {
            return json_object.areas[i].name;
        }
    }
    return ("not found")
}

// Поиск города по id в определенной области
function findCityInArea(area, city, json_object) {
    for (var i = 0; i <= json_object.areas.length - 1; i++) {
        if (json_object.areas[i].id == area) {
            for (var j = 0; j <= json_object.areas[i].areas.length - 1; j++) {
                if (json_object.areas[i].areas[j].id == city) {
                    return json_object.areas[i].areas[j].name;
                }
            }
        }
    }
    return ("not found")
}

//Поиск города по id по всем областям
function findCity(city, json_object) {
    for (var i = 0; i <= json_object.areas.length - 1; i++) {
        for (var j = 0; j <= json_object.areas[i].areas.length - 1; j++) {
            if (json_object.areas[i].areas[j].id == city) {
                return json_object.areas[i].areas[j].name + " (" + json_object.areas[i].name + ")";
            }
        }
    }
    return ("not found")
}

var services = document.querySelectorAll('.service-auto');
for (var i = 0; i < services.length; i++) {
    var service_id = services[i].dataset['id'];
    var area_id = services[i].dataset['area_id'];
    var city_id = services[i].dataset['city_id'];
    var area_name = findArea(area_id, json_object);
    var city_name = findCityInArea(area_id, city_id, json_object);
    if (area_name !== 'not found') {
        var area = document.getElementById('area_name_' + service_id);
        area.innerHTML = area_name;
    } else {
        area.innerHTML = '';
    }

    if (city_name !== 'not found') {
        var city = document.getElementById('city_name_' + service_id);
        city.innerHTML = city_name;
    }
}
    
   //выбор области
    if (select_area != 1 && !select_city != 1) {
         getAreas(json_object);
    }else{
        getAreasDefault(json_object);
    }
   

</script>
@endpush
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)

@section('content')
<div id="content" class="site-content page-services-auto">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <div class="section-top">
                    <h1 class="maincont-ttl">{{$page->title}}</h1>
                    <ul class="b-crumbs">
                        <li><a href="{{url('/')}}">Главная</a></li>
                        <li>{{$page->title}}</li>
                    </ul>
                    <div class="section-top-sort">
                        <form method="get" class="products-per-page1">
                            <select name="area" id="billing_area" class="sort-field city_select " autocomplete="area">
                                <option value="">Область</option>
                            </select>
                            <select name="city" id="billing_city" class="sort-field city_select " autocomplete="city">
                                <option value="">Город</option>
                            </select>
                            <select name="price" id="price" class="sort-field price_select " autocomplete="price">
                                <option value="">По цене</option>
                                <option value="asc" @if($price == 'asc') {{'selected'}} @endif>Дешевле</option>
                                <option value="desc" @if($price == 'desc') {{'selected'}} @endif>Дороже</option>
                            </select>
                            <select name="rating" id="rating" class="sort-field price_select " autocomplete="price">
                                <option value="">По рейтингу</option>
                                <option value="asc" @if($rating == 'asc') {{'selected'}} @endif>Рейтинг ниже</option>
                                <option value="desc" @if($rating == 'desc') {{'selected'}} @endif>Рейтинг выше</option>
                            </select>
                            <input id="search-go" type="submit" value="Поиск" class="sort-field">
                        </form>
                    </div>
                </div>

                <!-- To make Sidebar "Not Sticky" just remove  id="section-list-withsb" -->
                <div class="section-wrap-withsb">
                    <aside class="blog-sb-widgets section-sb" id="section-sb">
                        <div class="theiaStickySidebar">
                            <div class="section-filter">
                                <div class="section-filter">
                                    <div class="blog-sb-widget multishopcategories_widget">
                                        <h3 class="widgettitle">Категории</h3>
                                        <div class="section-sb-current">
                                            <ul class="section-sb-list">
                                                @include('partials.components.category-painting-sidebar')
                                            </ul>
                                        </div>
                                    </div>

                                    @include('partials.widget.last-articles')

                                </div>
                            </div>
                        </div>
                    </aside>        
                    <div class="section-list-withsb" id="section-list-withsb">
                        <div class="theiaStickySidebar">
                            @if($painters->count() > 0)
                                <div class="prod-litems section-list">
                                    @foreach($painters as $painter)
                                        <article class="prod-li sectls service-auto" data-id="{{$painter->id}}" data-area_id="{{$painter->area}}" data-city_id="{{$painter->city}}">
                                            <div class="prod-li-inner">
                                                <a href="{{url('/avtomalyar/'.$painter->id)}}" class="prod-li-img">
                                                    @if($painter->image)
                                                        <img src="{{Voyager::image($painter->image)}}" alt="">
                                                    @else
                                                        <img src="{{asset('img/spraygun.png')}}" alt="">
                                                    @endif
                                                </a>
                                                <div class="prod-li-cont">
                                                    <div class="prod-li-ttl-wrap">
                                                        <p>
                                                            <a href="javascript:void(0);">{{$category_detail->name}}</a>
                                                        </p>
                                                        <h3><a href="{{url('/avtomalyar/'.$painter->id)}}">{{$painter->company}}</a></h3>
                                                    </div><!--
                            No Space
                                                    --><div class="prod-li-prices">
                                                        <div class="prod-li-price-wrap wrap-painter">
                                                            <p class="price-d">Цена</p>
                                                            <p class="prod-li-price">от {{ $painter->{$category_detail->input_name_form} }} грн</p>
                                                        </div>

                                                    </div><!--
                                No Space
                                                    -->
                                                </div>

                                                <div class="prod-li-info">
                                                    @php
                                                        $rating = round($painter->reviews->avg('rating'), 1);
                                                    @endphp
                                                    <div class="prod-li-rating-wrap">
                                                        <p data-rating="{{floor($rating) ?? ''}}" class="prod-li-rating">
                                                            <i class="rating-ico" title="1"></i>
                                                            <i class="rating-ico" title="2"></i>
                                                            <i class="rating-ico" title="3"></i>
                                                            <i class="rating-ico" title="4"></i>
                                                            <i class="rating-ico" title="5"></i>
                                                        </p>
                                                        <p class="prod-li-rating-count">{{$rating ?? '0'}} из <a href="{{url('/avtomalyar/' .$painter->id). '/otzyvy'}}">{{$painter->reviews->count()}} (отзывов)</a></p>
                                                    </div> 
                                                    <div class="wrap-info">
                                                        <div class="prod-li-favorites">
                                                            <a data-blok="adress_{{$painter->id}}" href="javascript:void(0);" class="open-info hover-label add_to_wishlist"><i class="fa fa-send"></i><span>Написать</span></a>
                                                        </div>
                                                        <div class="prod-li-favorites">
                                                            <form method="POST" class="form-get-phone" action="{{url('/getPhone')}}">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{$painter->id}}">
                                                                <input type="hidden" name="master" value="painter">
                                                                <button type="submit" class="show-phone">Показать телефон</button>
                                                            </form>
                                                        </div>
                                                        <p class="prod-li-information">
                                                            <a data-blok="price_{{$painter->id}}" href="javascript:void(0);"  class="open-info hover-label"><i class="fa fa-tag"></i><span>Все цены</span></a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="page-styling prod-li-informations blok price_{{$painter->id}} price-informations">
                                                <div class="title-p">
                                                    <h4>Прайс лист</h4>
                                                </div>
                                                <dl class="prod-li-props">
                                                    @foreach($categories as $category)
                                                        @if($painter->{$category->input_name_form} >= 100)
                                                            <dt>{{$category->name}}:</dt>
                                                            <dd>от <a href="javascript:void(0);">{{ $painter->{$category->input_name_form} }}</a> грн</dd>
                                                        @endif
                                                    @endforeach
                                                </dl>
                                            </div>
                                            <div class="page-styling blok adress_{{$painter->id}} adress-informations"> 
                                                <div class="title-p">
                                                    <h4>Адрес</h4>
                                                    <p class="adress-text"><span id="area_name_{{$painter->id}}"></span>, <span id="city_name_{{$painter->id}}"></span>,  {{$painter->street}}, {{$painter->house}}</p>
                                                </div>
                                                <div class="form-validate modal-form partner-form" style="display: block;">
                                                    <form action="{{route('send_partner')}}" method="POST" class="form-validate">
                                                        @csrf
                                                        <h4>Напишите нам</h4>
                                                        <input type="hidden" name="id" value="{{$painter->id}}">
                                                        <input type="text" placeholder="Имя" data-required="text" name="name">
                                                        <input type="text" placeholder="Телефон" data-required="text" name="phone">
                                                        <input type="text" placeholder="email" data-required="text" data-required-email="email" name="email">
                                                        <textarea name="message" placeholder="Комментарий ..." data-required="text"></textarea>
                                                        <button class="btn1" type="submit"><i class="fa "></i> Отправить</button>
                                                        <p class="form-result">Сообщение отправлено!</p>
                                                        <p class="error-send">Сообщение не отправлено!</p>
                                                    </form>
                                                </div>
                                            </div>
                                        </article>
                                    @endforeach
                                </div>

                            {{ $painters->links('vendor.pagination.default') }}

                            @else
                            <h2 class="nothingfound">Ничего не найдено :(</h2>
                            @endif

                        </div><!-- .theiaStickySidebar -->
                    </div><!-- .section-list-withsb -->
                </div><!-- .section-wrap-withsb -->
            </div>
            <div class="cont maincont page-styling">
                <div class="page-cont">
                    <p class="text-page">{!!$page->body!!}</p>
                </div>
                
            </div>
            @if($page->faq)
                <div class="cont maincont page-styling">
                    <div class="page-cont">
                        <h2 class="mb35">Вопросы - Ответы</h2>
                        <p class="text-page">{!!$page->faq!!}</p>
                    </div>
                </div>        
            @endif  
        </main><!-- #main -->
    </div><!-- #primary -->

</div><!-- #content -->
@endsection