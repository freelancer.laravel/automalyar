
        <H2>Message from Academic Visa site.</H2>
    
        <table width="100%" border="0"  cellpadding="2" style="background-color: #E6E6E6">

            <tbody>
                @isset($feedback['create'])
                    <tr>
                        <td with="50">Created</td>
                        <td>{{ $createdPainter$feedback['create']}}</td>
                    </tr>
                @endisset
                @isset($feedback['current'])
                    <tr>
                        <td with="50">Current</td>
                        <td>{{ $feedback['current']}}</td>
                    </tr>
                @endisset
                @isset($feedback['first_name'])
                    <tr>
                        <td with="50">First Name</td>
                        <td>{{ $feedback['first_name']}}</td>
                    </tr>
                @endisset
                @isset($feedback['last_name'])
                    <tr>
                        <td with="50">Last Name</td>
                        <td>{{ $feedback['last_name']}}</td>
                    </tr>
                @endisset
                @isset($feedback['company'])
                  <tr>
                        <td with="50">Company</td>
                        <td>{{ $feedback['company'] }}</td>
                  </tr>      
                @endisset
                @isset($feedback['site'])
                  <tr>
                        <td with="50">Site</td>
                        <td>{{ $feedback['site'] }}</td>
                  </tr>      
                @endisset
                @isset($feedback['phone'])
                    <tr>
                        <td with="50">Phone</td>
                        <td>{{ $feedback['phone']}}</td>
                    </tr>
                @endisset
                @isset($feedback['email'])
                    <tr>
                        <td with="50">Email</td>
                        <td>{{ $feedback['email']}}</td>
                    </tr>
                @endisset

                
            </tbody>
        </table>


