@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)

@section('content')
<div id="content" class="site-content">

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">

                <div class="section-top">

                    <h1 class="maincont-ttl">Цены на покраску</h1>
                    <ul class="b-crumbs">
                        <li><a href="{{url('/')}}">Главная</a></li>
                        <li>Каталог автомаляров</li>
                    </ul>
                    <div class="section-top-sort">
                        <div class="section-view">
                            <p>View</p>
                            <div class="dropdown-wrap">
                                <p class="dropdown-title section-view-ttl">Gallery</p>
                                <ul class="dropdown-list">
                                    <li>
                                        <a href="catalog-list.html">List</a>
                                    </li>
                                    <li class="active">
                                        <a href="catalog-gallery.html">Gallery</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="section-sort">
                            <p>Sort</p>
                            <div class="dropdown-wrap">
                                <p class="dropdown-title section-sort-ttl">Newness</p>
                                <ul class="dropdown-list">
                                    <li>
                                        <a href="#">Popularity</a>
                                    </li>
                                    <li>
                                        <a href="#">Average rating</a>
                                    </li>
                                    <li class="active">
                                        <a href="#">Newness</a>
                                    </li>
                                    <li>
                                        <a href="#">Price: low to high</a>
                                    </li>
                                    <li>
                                        <a href="#">Price: high to low</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <form method="post" class="products-per-page">
                            <p>Per Page</p>
                            <select>
                                <option value="12" selected="selected">12</option>
                                <option value="24">24</option>
                                <option value="48">48</option>
                                <option value="-1">All</option>
                            </select>
                        </form>
                    </div>
                </div>

                <!-- To make Sidebar "Not Sticky" just remove  id="section-list-withsb" -->
                <div class="section-wrap-withsb">
                    <aside class="blog-sb-widgets section-sb" id="section-sb">
                        <div class="theiaStickySidebar">
                            <p class="section-filter-toggle filter_hidden">
                                <a href="#" id="section-filter-toggle-btn">Показать фильтр</a>
                            </p>
                            <div class="section-filter">
                                <div class="section-filter">
                                    <div class="blog-sb-widget multishopcategories_widget">
                                        <h3 class="widgettitle">Категории</h3>
                                        <div class="section-sb-current">
                                            <ul class="section-sb-list">
                                                @include('partials.components.category-sidebar')
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="blog-sb-widget multishopfeaturedproducts_widget">
                                        <h3 class="widgettitle">Блог</h3>
                                        <div class="products-featured-wrap">
                                            <div class="products-featured">
                                                <p class="products-featured-categ">
                                                    <a href="#">Tools</a>
                                                </p>
                                                <h5 class="products-featured-ttl"><a href="product.html">Rechargeable Battery</a></h5>
                                                <p class="products-featured-price">$201.00</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </aside>        
                    <div class="section-list-withsb" id="section-list-withsb">
                        <div class="theiaStickySidebar">

                            <div class="prod-litems section-list">
                                <article class="prod-li sectls">
                                    <div class="prod-li-inner">
                                        <a href="product.html" class="prod-li-img">
                                            <img src="{{asset('img/paintcar.png')}}" alt="">
                                        </a>
                                        <div class="prod-li-cont">
                                            <div class="prod-li-ttl-wrap">
                                                <p>
                                                    <a href="#">Lighting</a>
                                                </p>
                                                <h3><a href="product.html">Searchlight Portable</a></h3>
                                            </div><!--
                    No Space
                                            --><div class="prod-li-prices">
                                                <div class="prod-li-price-wrap wrap-painter">
                                                    <p>Price</p>
                                                    <p class="prod-li-price">1200 грн</p>
                                                </div>
                                                
                                            </div><!--
                        No Space
                                            --></div>
                                        <div class="prod-li-info">
                                            <div class="prod-li-rating-wrap">
                                                <p data-rating="5" class="prod-li-rating">
                                                    <i class="rating-ico" title="1"></i><i class="rating-ico" title="2"></i><i class="rating-ico" title="3"></i><i class="rating-ico" title="4"></i><i class="rating-ico" title="5"></i>
                                                </p>
                                                <p class="prod-li-rating-count">12</p>
                                            </div>
                                            <p class="prod-li-add">
                                                <a href="#" class="button hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to cart</span></a>
                                            </p>
                                            <p class="prod-li-compare">
                                                <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                            </p>
                                            <p class="prod-quickview">
                                                <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                            </p>
                                            <div class="prod-li-favorites">
                                                <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                            </div>
                                            <p class="prod-li-information">
                                                <a href="#" class="hover-label"><i class="icon ion-more"></i><span>Show Information</span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="page-styling prod-li-informations">

                                        <dl class="prod-li-props">
                                            <dt>Brand:</dt>
                                            <dd><a href="#">AIR</a></dd>
                                            <dt>Weight:</dt>
                                            <dd>1 kg</dd>
                                            <dt>Dimensions:</dt>
                                            <dd>4 x 50 cm</dd>
                                            <dt>Сolor:</dt>
                                            <dd><a href="#" rel="tag">Black</a>, <a href="#" rel="tag">Green</a></dd>
                                            <dt>Manufacturer:</dt>
                                            <dd><a href="#">France</a></dd>
                                            <dt>Material:</dt>
                                            <dd><a href="#" rel="tag">Metall</a>, <a href="#" rel="tag">Plastic</a>, <a href="#" rel="tag">Rubber</a></dd>
                                        </dl>
                                    </div>
                                </article>
                            </div>

                            <ul class="page-numbers">
                                <li><span class="page-numbers current">1</span></li>
                                <li><a class="page-numbers" href="#">2</a></li>
                                <li><a class="page-numbers" href="#">3</a></li>
                                <li><a class="page-numbers" href="#">4</a></li>
                                <li><a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div><!-- .theiaStickySidebar -->
                    </div><!-- .section-list-withsb -->
                </div><!-- .section-wrap-withsb -->

            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

</div><!-- #content -->
@endsection