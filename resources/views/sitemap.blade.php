@extends('layouts.master')
@section('title', 'Карта сайта')
@section('content')
<div id="content" class="site-content page-services-auto">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <div class="section-top">
                    <h1 class="maincont-ttl">Карта сайта</h1>
                    <ul class="b-crumbs">
                        <li><a href="{{url('/')}}">Главная</a></li>
                        <li>Карта сайта</li>
                    </ul>
                </div>
                <div class="container-fluid blog-sb-widgets page-styling site-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-3 align-center-mobile widget">
                            <h3 class="widgettitle">Страницы</h3>
                            <ul class="menu">
                                @foreach($pages as $page)
                                    <li>
                                        <a href="{{url($page->slug)}}">{{$page->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 align-center-mobile widget">
                            <h3 class="widgettitle">Категории</h3>
                            <ul class="menu">
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{url('/pokraska/' .$category->slug)}}">{{$category->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 align-center-mobile widget">
                            <h3 class="widgettitle">Статьи</h3>
                            <ul class="menu">
                                @foreach($posts as $post)
                                    <li>
                                       <a href="{{url('/post/' .$post->slug)}}">{{$post->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-3 align-center-mobile widget">
                            <h3 class="widgettitle">Автомаляры</h3>
                            <ul class="menu">
                                @foreach($painters as $painter)
                                    <li>
                                       <a href="{{url('/avtomalyar/' .$painter->id)}}">{{$painter->company}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
