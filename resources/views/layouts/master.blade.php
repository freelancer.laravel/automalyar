<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <!--<![endif]-->
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{setting('site.google_analytics_tracking_id')}}"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '{{setting('site.google_analytics_tracking_id')}}');
        </script>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>@yield('title')</title>
        <meta name="keywords" content="@yield('meta_keyword')">
        <meta name="description" content="@yield('meta_description')">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/additional.css')}}">
        <link rel="stylesheet" href="{{ asset('css/elements.css') }}">
        <link rel="stylesheet" href="{{ asset('css/media.css')}}">
        <link rel="stylesheet" href="{{ asset('css/elements-media.css') }}">

    </head>
    <body>
        <div class="preloader">
            <div class="preloader__row">
                <div class="preloader__item"></div>
                <div class="preloader__item"></div>
            </div>
        </div>
        <div id="page" class="site">

            @include('partials.header')

            @yield('content')

            @include('partials.footer')

            <script src="{{ asset('js/jquery.min.js') }}"></script>
            <script src="{{ asset('js/jquery-plugins.js') }}"></script>
            <!-- Main JS -->
            <script src="{{ asset('js/main.js') }}"></script>

            @stack('scripts')

            <script type="text/javascript">
                window.onload = function () {
                    document.body.classList.add('loaded_hiding');
                    window.setTimeout(function () {
                        document.body.classList.add('loaded');
                        document.body.classList.remove('loaded_hiding');
                    }, 500);
                }
            </script>

            <!-- End JavaScript -->
            @if (session('swal_status'))
            <script>
                swal.fire({!! session('swal_status') !!});
            </script>
            <script>
                if ({!! session('swal_status') !!}['icon'] == 'success') {
                $('#messAudio')[0].play();
                }
            </script>

            @endif
        </div>
    </body>
</html>
