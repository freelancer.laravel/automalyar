@extends('layouts.master')
@push ('scripts')
<script type="text/javascript">
    var select_area = {{$area}};
    var select_city = {{$city}};
    //alert(select_area)

function Get(yourUrl) {
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET", yourUrl, false);
    Httpreq.send(null);
    return Httpreq.responseText;
}
var linkCityJson = '{{url('/')}}/Json/city-ua.json'
var json_object = JSON.parse(Get(linkCityJson));

var area_obj = $('#area_name');
var city_obj = $('#city_name');

var area_id = area_obj.attr('data_area_id');
var city_id = city_obj.attr('data_city_id');


// Поиск области по id
function findArea(area, json_object) {
    for (var i = 0; i <= json_object.areas.length - 1; i = i + 1) {
        if (json_object.areas[i].id == area) {
            return json_object.areas[i].name;
        }
    }
    return ("not found")
}

//Поиск города по id по всем областям
function findCity(city, json_object) {
    for (var i = 0; i <= json_object.areas.length - 1; i++) {
        for (var j = 0; j <= json_object.areas[i].areas.length - 1; j++) {
            if (json_object.areas[i].areas[j].id == city) {
                return json_object.areas[i].areas[j].name + " (" + json_object.areas[i].name + ")";
            }
        }
    }
    return ("not found")
}
//Вставка области и города на страницу
function insertAreaCity(area_obj, city_obj, area_id, city_id, json_object) {
    var area_name = findArea(area_id, json_object);
    //console.log(area_id)
    if(area_name != 'not found'){
        area_obj.text(area_name);
    }

    var city_name = findCity(city_id, json_object);

    if(city_name != 'not found'){
        city_obj.text(city_name);
    }

}

insertAreaCity(area_obj, city_obj, area_id, city_id, json_object);


</script>
@endpush
@section('title', 'Покраска авто ' .$painter->company)

@section('content')
<div id="content" class="site-content painter">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cont maincont">
                <h1 class="maincont-ttl">Покраска авто {{$painter->company}}</h1>
                <ul class="b-crumbs">
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li><a href="{{route('painting')}}">Покраска авто</a></li>
                    <li>{{$painter->company}}</li>
                </ul>
                <article>
                    <div class="prod">
                        <div class="prod-slider-wrap prod-slider-shown">
                            <div class="flexslider1 prod-slider" id="prod-slider">
                                <ul class="slides">
                                    <li>
                                        <a data-fancybox-group="prod" class="fancy-img1">

                                            @if($painter->image)
                                                <img src="{{Voyager::image($painter->image)}}" alt="">
                                            @else
                                                <img src="{{asset('img/spraygun.png')}}" alt="">
                                            @endif
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="prod-cont">
                            <div class="prod-rating-wrap">
                                <p data-rating="{{$middleStar}}" class="prod-rating">
                                    <i class="rating-ico" title="1"></i>
                                    <i class="rating-ico" title="2"></i>
                                    <i class="rating-ico" title="3"></i>
                                    <i class="rating-ico" title="4"></i>
                                    <i class="rating-ico" title="5"></i>
                                </p>
                                <p class="prod-rating-count"><span class="middle-star-num">{{$middleStarNum ?? '0'}}</span> из <span class="quantity">{{$numberReviews}}</span> (отзывов)</p>
                            </div>

                            <h2 class="prod-title">Прайс лист</h2>
                            <div class="prod-props">
                                <dl class="product_meta">
                                    @if($categories)
                                        @foreach($categories as $category)
                                            @if($painter->{$category->input_name_form} >= 1)
                                                <dt>{{$category->name}}:</dt>
                                                <dd>от <a href="javascript:void(0);">{{ $painter->{$category->input_name_form} }}</a> грн</dd>
                                            @endif
                                        @endforeach
                                    @endif
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="prod-tabs-wrap">
                        <ul class="prod-tabs">
                            <li id="prod-desc" class="active" data-prodtab-num="1">
                                <a data-prodtab="#prod-tab-1" href="#">О нас</a>
                            </li>
                            <li data-prodtab-num="2" id="prod-props">
                                <a data-prodtab="#prod-tab-2" href="#">Контакты</a>
                            </li>
                            <li data-prodtab-num="3" id="prod-reviews">
                                <a data-prodtab="#prod-tab-3"  href="#">Отзывы (<span class="quantity">{{$numberReviews}}</span>)</a>
                            </li>
                            <li class="prod-tabs-addreview">Добавить отзыв</li>
                        </ul>
                        <div class="prod-tab-cont">
                            <p data-prodtab-num="1" class="prod-tab-mob active" data-prodtab="#prod-tab-1">О нас</p>
                            <div class="prod-tab page-styling prod-tab-desc" id="prod-tab-1">
                                <p>{{$painter->about}}</p>
                            </div>
                            <p data-prodtab-num="2" class="prod-tab-mob" data-prodtab="#prod-tab-2">Контакты</p>
                            <div class="prod-tab" id="prod-tab-2">
                                <div class="title-p">
                                    <h4>Адрес</h4>
                                    <p class="adress-text"><span id="area_name" data_area_id="{{$painter->area}}"></span>, <span id="city_name" data_city_id="{{$painter->city}}"></span>,  {{$painter->street}}, {{$painter->house}}</p>
                                    <form method="POST" class="form-get-phone" action="{{url('/getPhone')}}">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$painter->id}}">
                                        <input type="hidden" name="master" value="painter">
                                        <button type="submit" class="show-phone">Показать телефон</button>
                                    </form>

                                </div>
                                <div class="form-validate modal-form partner-form" style="display: block;">
                                    <form action="{{route('send_partner')}}" method="POST" class="form-validate">
                                        @csrf
                                        <h4>Напишите нам</h4>
                                        <input type="hidden" name="id" value="{{$painter->id}}">
                                        <input type="text" placeholder="Имя" data-required="text" name="name">
                                        <input type="text" placeholder="Телефон" data-required="text" name="phone">
                                        <input type="text" placeholder="email" data-required="text" data-required-email="email" name="email">
                                        <textarea name="message" placeholder="Комментарий ..." data-required="text"></textarea>
                                        <button class="btn1" type="submit"><i class="fa "></i> Отправить</button>
                                        <p class="form-result">Сообщение отправлено!</p>
                                        <p class="error-send">Сообщение не отправлено!</p>
                                    </form>
                                </div>
                            </div>
                            <p data-prodtab-num="3" class="prod-tab-mob" data-prodtab="#prod-tab-3">Отзывы ({{$numberReviews}})</p>
                            <div class="prod-tab prod-reviews" id="prod-tab-3">
                                <div id="reviews">
                                    <div id="review_form_wrapper" class="prod-addreview-form">
                                        <div id="review_form">
                                            <div id="respond" class="comment-respond">
                                                <p class="prod-tab-addreview" id="reply-title">Отзыв о компании</p>
                                                <form action="{{route('review')}}" method="post" id="commentform" class="review-form form-validate">
                                                    @csrf
                                                    <div class="comment-form-rating">
                                                        <p class="stars">
                                                            <span>
                                                                1<input type="radio"  class="star-1" name="rating" value="1" required>
                                                                2<input type="radio"  class="star-2" name="rating" value="2">
                                                                3<input type="radio"  class="star-3" name="rating" value="3">
                                                                4<input type="radio"  class="star-4" name="rating" value="4">
                                                                5<input type="radio"  class="star-5" name="rating" value="5">
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <input type="hidden" name="reviewable_type" value="painter">
                                                    <input type="hidden" name="reviewable_id" value="{{$painter->id}}">
                                                    <input type="text" name="name" placeholder="Имя" data-required="text">
                                                    <input type="text" name="email" placeholder="Email" data-required="text" data-required-email="email">
                                                    <textarea name="description" placeholder="Текст отзыва .." data-required="text"></textarea>
                                                    <button class="submit" type="submit"><i class="fa"></i> Отправить</button>
                                                    <p class="form-result">Отзыв добавлен!</p>
                                                    <p class="error-send">Отзыв не добавлен!</p>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <ul id="comments" class="review-list">
                                        @foreach($reviews as $review)
                                            <li class="prod-review">
                                                <h3>{{$review->name}}</h3>
                                                <p class="prod-review-rating" title="Rated 5 out of 5">
                                                    @foreach(range(1, $review->rating) as $number)
                                                        <i class="rating-ico"></i>
                                                    @endforeach
                                                    <time>{{$review->created_at->format('j, F, Y, H : i')}}</time>
                                                </p>
                                                <p>{{$review->description}}</p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </article>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

</div><!-- #content -->

@endsection
