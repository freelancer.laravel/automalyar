@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)

@section('content')
<div id="content" class="site-content">
    <div id="primary" class="content-area width-full">
        <main id="main" class="site-main">
            <div class="maincont page-styling page-full">
                <div class="heroblock" style="background-image: url({{ Voyager::image($page->image) }});">
                    <div class="mask"></div>
                    <p class="heroblock-subttl"><a href="{{url('pokraska')}}">Доска объявлений автосервисных услуг</a></p>
                    <h3 class="heroblock-ttl">Цены на покраску авто в городах Украины</h3>
                    <a href="{{url('pokraska')}}" class="btn">Выбрать автомаляра</a>
                </div>
                <div class="cont row-wrap-boxed">
                    <h1 class="mb30 maincont-ttl">{{$page->title}}</h1>
                    <div class="page-cont">
                        <h2 class="mb35">Детали кузова</h2>
                        <div class="row">
                            @foreach($categories as $category)
                                <div class="cf-sm-6 cf-lg-3 col-sm-6 col-md-3 category-item">
                                    <a href="{{url('pokraska/' .$category->slug)}}">
                                        <div class="team-i style_3">
                                            <p class="team-i-img">
                                                <img src="{{ Voyager::image($category->image) }}" alt="{{$category->name}} цена покраски" />
                                                <span class="mask-category"></span>
                                            </p>
                                            <h3>{{$category->name}}</h3>
                                            <p class="team-i-position">от {{$painters->min($category->input_name_form)}} грн</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="cont row-wrap-boxed">
                    <div class="page-cont">
                        <h2 class="mb35">Покраска авто</h2>
                        <p class="text-page">{!!$page->body!!}</p>
                    </div>
                </div>
                @if($page->faq)
                <div class="cont row-wrap-boxed">
                    <div class="page-cont">
                        <h2 class="mb35">Вопросы - Ответы</h2>
                        <p class="text-page">{!!$page->faq!!}</p>
                    </div>
                </div>
                @endif
            </div><!-- .maincont.page-styling.page-full -->
        </main><!-- #main -->
    </div><!-- #primary -->    </div><!-- #content -->
@endsection