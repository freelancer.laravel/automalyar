<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    const ALLOWED_MASTER_NAMES = ['painter', 'picker'];

    protected $guarded = [];

    public function reviewable()
    {
        return $this->morphTo();
    }
}

