<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Painter extends Model {

    use HasFactory;

    protected $guarded = ['user_id', 'vip', 'tariff'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function save(array $options = []) {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->user_id = Auth::user()->getKey();
        }

        return parent::save();
    }

    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }


    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute() {
        return "{$this->first_name} {$this->last_name}";
    }

}
