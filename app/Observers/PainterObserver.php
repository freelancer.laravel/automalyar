<?php

namespace App\Observers;

use App\Models\Painter;
use App\Models\User;
use Notification;
use App\Notifications\CreatedPainterNotification;
use App\Notifications\UpdatedPainterNotification;

class PainterObserver
{
    /**
     * Handle the Painter "created" event.
     *
     * @param  \App\Models\Painter  $painter
     * @return void
     */
    public function created(Painter $painter)
    {
        $admin = User::where('role_id', 1)->first();
        
        Notification::send($admin, new CreatedPainterNotification($painter));
    }

    /**
     * Handle the Painter "updated" event.
     *
     * @param  \App\Models\Painter  $painter
     * @return void
     */
    public function updated(Painter $painter)
    {
        $admin = User::where('role_id', 1)->first();
        
        Notification::send($admin, new UpdatedPainterNotification($painter));
    }

    /**
     * Handle the Painter "deleted" event.
     *
     * @param  \App\Models\Painter  $painter
     * @return void
     */
    public function deleted(Painter $painter)
    {
        //
    }

    /**
     * Handle the Painter "restored" event.
     *
     * @param  \App\Models\Painter  $painter
     * @return void
     */
    public function restored(Painter $painter)
    {
        //
    }

    /**
     * Handle the Painter "force deleted" event.
     *
     * @param  \App\Models\Painter  $painter
     * @return void
     */
    public function forceDeleted(Painter $painter)
    {
        //
    }
}
