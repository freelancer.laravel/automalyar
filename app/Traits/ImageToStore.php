<?php

namespace App\Traits;

use Image;

/**
 * Save image in storage and put path image to DB
 *
 * @param $request
 * @return string path image from DB else empty
 */
trait ImageToStore {
    
    public function saveImage($request) {
        
        $old_image = $request->input('old_image');
        
        $image = $request->file('image');
        
        if ($image) {
            
            $input['imagename'] = time() . '.' . $image->extension();

            $destinationPath = public_path('/storage/uploads');

            $img = Image::make($image->path());

            $img->fit(400)->save($destinationPath . '/' . $input['imagename']);

            return 'uploads/' . $input['imagename'];
            
        } else {
            
            if ($old_image) {
                
                return $old_image;
                
            } else {
                
                return '';
                
            }
            return '';
        }
    }
}
