<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\SendEmailToAdminEvent;
use App\Jobs\SendToMailJob;

class SendEmailToAdminListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendEmailToAdminEvent $event)
    {
        $job = new SendToMailJob($event->contactEvent, $event->contactEmailsEvent);

        dispatch($job);
    }
}
