<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\SendEmailToPartnerEvent;
use App\Jobs\SendEmailToPartnerJob;

class SendEmailToPartnerListener implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
     public function handle(SendEmailToPartnerEvent $event)
    {
        $job = new SendEmailToPartnerJob($event->contactEvent, $event->contactEmailsEvent);

        dispatch($job);
    }
}
