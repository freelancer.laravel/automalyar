<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\ContactRequestInformation;
use Illuminate\Support\Facades\Mail;

class SendToMailJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $contactJob;
    
    public $contactEmailsJob;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($contactJob, $contactEmailsJob)
    {
        $this->contactJob = $contactJob;
        
        $this->contactEmailsJob = $contactEmailsJob;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(explode(', ', $this->contactEmailsJob))
                ->send(new ContactRequestInformation($this->contactJob));
    }
}
