<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UpdatedPainterNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $updatedPainter;
    
    public $tries = 3;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($updatedPainter)
    {
        $this->updatedPainter = $updatedPainter;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Страница автомаляря обновлена:')
                    ->line($this->updatedPainter->getFullNameAttribute())
                    ->line($this->updatedPainter->phone)
                    ->line($this->updatedPainter->email)
                    ->action('Посмотреть страницу Автомаляря', url('/avtomalyar/' . $this->updatedPainter->id))
                    ->line('С уважением команда ' . setting('site.title') . '!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
