<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\NexmoMessage;

class CreatedPainterNotification extends Notification implements ShouldQueue {

    use Queueable;

    public $tries = 3;
    public $createdPainter;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($createdPainter) {
        $this->createdPainter = $createdPainter;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['nexmo', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)
                        ->line('Зарегистрирован новый автомаляр:')
                        ->line($this->createdPainter->getFullNameAttribute())
                        ->line($this->createdPainter->phone)
                        ->line($this->createdPainter->email)
                        ->action('Посмотреть страницу Автомаляря',
                                url('/avtomalyar/' . $this->createdPainter->id))
                        ->line('С уважением команда ' . setting('site.title') . '!');
    }

    /**
     * Get the Vonage / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmo($notifiable) {
        return (new NexmoMessage)
                        ->content('Зарегистрирован новый автомаляр '
                                . 'id: '
                                . $this->createdPainter->id)
                        ->unicode();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        return [
                //
        ];
    }

}
