<?php

namespace App\Repositories;

use App\Models\Painter;
use Illuminate\Support\Facades\DB;

class PaintersRepository
{
    /**
     * @params $paint_detail, $rating, $city, $price, $sort_field
     * @return \Illuminate\Support\Collection
     */
    public function getPaintersSortPaginate($paint_detail, $rating, $city, $price, $sort_field)
    {
        return Painter::where('status', '1')
            ->where($paint_detail, '>=', 100)
            ->when($rating, function ($query) use ($rating) {
                return $query
                    ->leftJoin("reviews", "painters.id", "=", "reviews.painter_id")
                    ->select(DB::raw('painters.*'))
                    ->groupByRaw('painters.id, painters.user_id, painters.status, '
                        . 'painters.vip, painters.tariff, painters.ip_adress, painters.first_name, '
                        . 'painters.last_name, painters.email, painters.phone, painters.company, painters.about, '
                        . 'painters.site, painters.country, painters.area, painters.city, '
                        . 'painters.street, painters.house, painters.image, painters.phone_view, '
                        . 'painters.bamper_front, painters.hood, painters.wing_front, painters.roof, '
                        . 'painters.door_front, painters.door_rear, painters.trunk_lid, painters.wing_rear, '
                        . 'painters.bumper_rear, painters.threshold, painters.mirror, painters.part_local, '
                        . 'painters.car_full, painters.created_at, painters.updated_at')
                    ->orderByRaw('AVG(reviews.rating)' . $rating);
            })

            ->when($city, function ($query) use ($city) {
                return $query->where('city', $city);
            })
            ->when($price, function ($query) use ($price, $sort_field) {
                return $query->orderBy($sort_field, $price);
            })
            ->paginate(10);
    }
}
