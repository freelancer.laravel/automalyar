<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Category;
use App\Models\Post;
use App\Models\Painter;

class SitemapController extends Controller
{
    
    public function index(Request $request)
    {
        $pages = Page::where('status', 'ACTIVE')->get();
        $posts = Post::where('status', 'PUBLISHED')->get();
        $categories = Category::where('parent_id', '!=', null)->get();
        $painters = Painter::where('status', '1')->get();

        return response()->view('sitemap_xml', compact('pages', 'posts', 'categories', 'painters'))
          ->header('Content-Type', 'text/xml');
    }
    
    public function sitemap(Request $request)
    {
        $pages = Page::where('status', 'ACTIVE')->get();
        $posts = Post::where('status', 'PUBLISHED')->get();
        $categories = Category::where('parent_id', '!=', null)->get();
        $painters = Painter::where('status', '1')->get();

        return view('sitemap', compact('pages', 'posts', 'categories', 'painters'));
    }
}