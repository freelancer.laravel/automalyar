<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * @param CommentStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CommentStoreRequest $request) {

        $dataValid = $request->validated();

        try {

            if (setting('admin.moderate_comments')) {
                $status = 0;
            } else {
                $status = 1;
            }

            $dataValid['ip_adress'] = $request->ip();

            $dataValid['status'] = $status;

            $createdComment = Comment::create($dataValid);

        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => 'Данные не сохранились!'
            ]);
        }

        $data = [
            'status' => true,
            'data' => [
                'id' => $createdComment->id,
                'name' => $createdComment->name,
                'message' => $createdComment->message,
                'created_at' => $createdComment->created_at->format('j, F, Y, H : i')
            ]

        ];

        return response()->json($data);
    }
}
