<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Painter;
use App\Models\Category;
use App\Http\Requests\PainterStoreRequest;
use App\Http\Requests\PainterUpdateRequest;
use App\Traits\ImageToStore;

class PainterController extends Controller {

    use ImageToStore;

    public function __construct() {

        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $user_id = Auth::user()->id;

        $entry = Painter::where('user_id', $user_id)->first();

        //if isset entry from user
        if ($entry) {
            $entry = $entry;
        } else {
            $entry = false;
        }

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {

            return redirect()->route('cabinet');

        }

        $categories = $parent_category->children()->orderBy('order')->get();

        return view('admin.cabinet', compact('entry', 'user_id', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $user = Auth::user();

        $entry = Painter::where('user_id', $user->id)->first();

        //if isset entry from user
        if ($entry) {
            return redirect()->route('cabinet_advt_edit', ['id' => $user->id]);
        } else {

            $parent_category = Category::where('slug', 'kuzov')->first();

            if (!$parent_category) {
                return redirect()->route('cabinet');
            }

            $categories = $parent_category->children()->orderBy('order')->get();

            return view('admin.advertisement_create', compact('user', 'categories'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PainterStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PainterStoreRequest $request) {

        $dataValid = $request->validated();

        try {

            $pathImage = $this->saveImage($request);

            $dataValid['image'] = $pathImage;

            $dataValid['ip_adress'] = $request->ip();

            $dataValid['status'] = 1;

            $createdPainter = Painter::create($dataValid);

        } catch (\Exception $e) {

            return \Redirect::back()->with('error', 'Данные не сохранились обратитесь в поддержку');
        }

        return redirect()->route('cabinet_advt_edit',
                ['id' => $createdPainter->id])
                ->with('success', 'Данные успешно сохранены!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $user = Auth::user();

        $entry = Painter::where('user_id', $user->id)->first();

        //if isset entry from user
        if (!$entry) {
            return redirect()->route('cabinet_advt_create');
        }

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            return redirect()->route('cabinet');
        }

        $categories = $parent_category->children()->orderBy('order')->get();

        return view('admin.advertisement_edit', compact('user', 'categories', 'entry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PainterUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PainterUpdateRequest $request, $id) {

        $dataValid = $request->validated();

        try {

            $pathImage = $this->saveImage($request);

            $dataValid['image'] = $pathImage;

            $dataValid['ip_adress'] = $request->ip();

            $painter = Painter::where('id', $id)->first();

            $painter->update($dataValid);

        } catch (\Exception $e) {

            return \Redirect::back()
                    ->with('error', 'Данные не сохранились обратитесь в поддержку');
        }

        return redirect()->route('cabinet_advt_edit',
                ['id' => $painter->id])
                ->with('success', 'Данные успешно сохранены!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

    }

}
