<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewStorerequest;
use App\Models\Page;
use App\Models\Review;

class ReviewController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::where('slug', 'otzyvy')->first();

        if (!$page) {
            return abort(404);
        }

        $reviews = Review::orderBy('order')->get();

        return view('reviews', compact('page', 'reviews'));

    }

    /**
     * @param ReviewStorerequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReviewStorerequest $request)
    {
        $dataValid = $request->validated();

        try {
            $modelName = ucfirst($request->input('reviewable_type'));

            $master_id = $request->input('reviewable_id');

            $modelNamespace = 'App\\Models\\' . $modelName;

            $dataValid['ip_adress'] = $request->ip();

            $dataValid['reviewable_id'] = $master_id;

            $dataValid['reviewable_type'] = $modelNamespace;

            $createdReview = Review::create($dataValid);

        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => 'Данные не сохранились!'
            ]);
        }

        $reviews = $createdReview->reviewable()->reviews()->get();

        $numberReviews = $reviews->count();

        $middleStarNum = round($reviews->avg('rating'), 1);

        $middleStar = floor($reviews->avg('rating'));

        $data = [
            'status' => true,
            'data' => [
                'id' => $createdReview->id,
                'rating' => $createdReview->rating,
                'name' => $createdReview->name,
                'description' => $createdReview->description,
                'created_at' => $createdReview->created_at->format('j, F, Y, H : i'),
                'numberReviews' => $numberReviews,
                'middleStarNum' => $middleStarNum,
                'middleStar' => $middleStar,
            ]
        ];

        return response()->json($data);
    }
}
