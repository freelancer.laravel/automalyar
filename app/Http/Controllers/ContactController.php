<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Painter;
use App\Events\SendEmailToAdminEvent;
use App\Events\SendEmailToPartnerEvent;
use App\Http\Requests\ContactStoreRequest;

class ContactController extends Controller
{
    /**
     * @param ContactStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(ContactStoreRequest $request)
    {
        $dataValid = $request->validated();

        try {

            $dataValid['ip_adress'] = $request->ip();

            $createdContact = Contact::create($dataValid);

            $emails = setting('site.request_emails');

            event(new SendEmailToAdminEvent($createdContact, $emails));

        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => 'Сообщение не отправлено!'
            ]);

        }

        return response()->json([
            'status' => true,
            'data' => $createdContact
        ]);
    }

    public function send_partner(ContactStoreRequest $request)
    {
        $dataValid = $request->validated();

        try {

            $entry = Painter::where('id', $request->input('id'))->first();

            $emails = setting('site.request_emails') .', '. $entry->email;

            $dataValid['ip_adress'] = $request->ip();

            $createdContact = Contact::create($dataValid);

            event(new SendEmailToPartnerEvent($createdContact, $emails));

        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => 'Сообщение не отправлено!'
            ]);

        }

        return response()->json([
            'status' => true,
            'data' => $createdContact
        ]);

    }


}
