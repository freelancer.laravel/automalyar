<?php

namespace App\Http\Controllers;

use App\Repositories\PaintersRepository;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Category;
use App\Models\Painter;
use App\Models\Post;

class PageController extends Controller
{
    public const DEFAULT_AREA = 1;
    public const DEFAULT_CITY = 1;
    public const DEFAULT_CATEGORY_DETAIL = 'bamper-perednij';

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::where('slug', '/')->first();

        if (!$page) {
            return abort(404);
        }

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            $categories = $parent_category->children()->orderBy('order')->get();
        }

        $painters = Painter::where('status', '1')->get();

        return view('homepage', compact('page', 'categories', 'painters'));

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param PaintersRepository $paintersRepository
     * @return \Illuminate\Http\Response
     */
    public function painting(Request $request, PaintersRepository $paintersRepository)
    {
        $slug = self::DEFAULT_CATEGORY_DETAIL;

        $area = $request->area;

        $city = $request->city;

        $price = $request->price;

        $rating = $request->rating ;

        if ($rating === 'asc' || $rating === 'desc' ) {
            $rating = $rating;
        } else {
            $rating = false;
        }

        $page = Page::where('slug', 'pokraska')->first();

        $posts = Post::where('status', 'PUBLISHED')->latest('created_at')->limit(3)->get();

        if (!$page) {
            return abort(404);
        }

        //текущая категория
        $category_detail = Category::where('slug', $slug)->first();

        if (!$category_detail) {
            return abort(404);
        }

        $sort_field = $category_detail->input_name_form;

        $paint_detail = $sort_field;

        $painters = $paintersRepository->getPaintersSortPaginate($paint_detail, $rating, $city, $price, $sort_field);

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            $categories = $parent_category->children()->orderBy('order')->get();
        }

        if ($area) {
            $area = $area;
        }else{
            $area = self::DEFAULT_AREA;
        }

        if ($city) {
            $city = $city;
        }else{
            $city = self::DEFAULT_CITY;
        }

        return view('painting', compact('page', 'posts', 'category_detail',
                'categories', 'painters', 'area', 'city', 'price', 'rating'));

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param PaintersRepository $paintersRepository
     * @return \Illuminate\Http\Response
     */
    public function painting_category(Request $request, $slug, PaintersRepository $paintersRepository)
    {
        $area = $request->area;

        $city = $request->city;

        $price = $request->price;

        $rating = $request->rating ;

        if ($rating === 'asc' || $rating === 'desc' ) {
            $rating = $rating;
        } else {
            $rating = false;
        }

        //текущая Page категория
        $page = Category::where('slug', $slug)->first();

        if (!$page) {
            return abort(404);
        }

        $category_detail = $page;

        $sort_field = $category_detail->input_name_form;

        $paint_detail = $sort_field;

        $posts = Post::where('status', 'PUBLISHED')->latest('created_at')->limit(3)->get();

        $painters = $paintersRepository->getPaintersSortPaginate($paint_detail, $rating, $city, $price, $sort_field);

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            //категории меню слева
            $categories = $parent_category->children()->orderBy('order')->get();
        }

        if ($area) {
            $area = $area;
        }else{
            $area = self::DEFAULT_AREA;
        }

        if ($city) {
            $city = $city;
        }else{
            $city = self::DEFAULT_CITY;
        }

        return view('painting_category',
                compact('page', 'posts', 'category_detail', 'categories',
                        'painters', 'area', 'city', 'price', 'rating'));

    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function blog()
    {
        $page = Page::where('slug', 'blog')->first();

        if (!$page) {
            return abort(404);
        }

        $posts = Post::where('status', 'PUBLISHED')->paginate(10);

        return view('blog', compact('page', 'posts'));
    }

    /**
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function post($slug)
    {
        $page = Post::where('slug', $slug)->where('status', 'PUBLISHED')->first();

        if (!$page) {
            return abort(404);
        }

        $comments = $page->comments()->where('status', 1)->get();

        $current_url = url()->current();

        return view('post', compact('page', 'comments', 'current_url'));
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function terms()
    {
        $page = Page::where('slug', 'usloviya-ispolzovaniya')->first();

        if (!$page) {
            return abort(404);
        }

        return view('terms', compact('page'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function painter($id)
    {
        $painter = Painter::where('id', $id)->first();

        if (!$painter) {
            return abort(404);
        }

        $area = $painter->area;

        $city = $painter->city;

        $parent_category = Category::where('slug', 'kuzov')->first();

        if (!$parent_category) {
            $categories = false;
        }else{
            $categories = $parent_category->children()->orderBy('order')->get();
        }

        $reviews = $painter->reviews;

        $numberReviews = $reviews->count();

        $middleStarNum = round($reviews->avg('rating'), 1);

        $middleStar = floor($reviews->avg('rating'));


        return view('painter',
                compact('painter', 'categories', 'reviews', 'numberReviews',
                        'middleStarNum', 'middleStar', 'area', 'city'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function review_painter($id)
    {
        $painter = Painter::where('id', $id)->first();

        if (!$painter) {
            return abort(404);
        }

        $reviews = $painter->reviews;

        $numberReviews = $reviews->count();

        $middleStarNum = round($reviews->avg('rating'), 1);

        $middleStar = floor($reviews->avg('rating'));

        return view('reviews',
                compact('painter', 'reviews', 'numberReviews', 'middleStarNum',
                        'middleStar'));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhone(Request $request)
    {
        $masterName = $request->master;

        $id = $request->id;

        if ($masterName == 'painter') {
            $master = Painter::where('id', $id)->first();
        }

        if (!$master) {
            return response()->json(['status' => false]);
        }

        $master->phone_view++;

        $master->save();

        $data = [
            'status' => true,
            'data' => ['phone' => $master->phone]
        ];

        return response()->json($data);
    }

}
