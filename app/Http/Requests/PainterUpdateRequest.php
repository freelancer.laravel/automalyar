<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PainterUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'about' => 'required|string|max:255',
            'site' => 'string|max:255|nullable',
            'country' => 'required|string|max:255',
            'area' => 'required|numeric',
            'city' => 'required|numeric',
            'street' => 'required|string|max:255',
            'house' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|unique:painters,email,'.$this->id,
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //service
            'bamper_front' => 'numeric|nullable',
            'hood' => 'numeric|nullable',
            'wing_front' => 'numeric|nullable',
            'roof' => 'numeric|nullable',
            'door_front' => 'numeric|nullable',
            'door_rear' => 'numeric|nullable',
            'trunk_lid' => 'numeric|nullable',
            'wing_rear' => 'numeric|nullable',
            'bumper_rear' => 'numeric|nullable',
            'part_local' => 'numeric|nullable',
            'threshold' => 'numeric|nullable',
            'mirror' => 'numeric|nullable',
            'car_full' => 'numeric|nullable',
        ];
    }
}
