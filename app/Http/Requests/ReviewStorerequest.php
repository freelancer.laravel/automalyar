<?php

namespace App\Http\Requests;

use App\Models\Review;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ReviewStorerequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reviewable_type' => ['required', 'max:255', Rule::in(Review::ALLOWED_MASTER_NAMES)],
            'reviewable_id' => 'required|integer',
            'rating' => 'required|integer',
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'description' => 'required|max:1000',
        ];
    }
}
