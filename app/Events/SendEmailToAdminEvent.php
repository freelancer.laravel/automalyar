<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\Contact;

class SendEmailToAdminEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $contactEvent;
    
    public $contactEmailsEvent;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($contactEvent, $contactEmailsEvent)
    {
        $this->contactEvent = $contactEvent;
        
         $this->contactEmailsEvent = $contactEmailsEvent;
    }

    
}
