<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Models\Painter;
use App\Observers\PainterObserver;
use App\Events\SendEmailToAdminEvent;
use App\Events\SendEmailToPartnerEvent;
use App\Listeners\SendEmailToAdminListener;
use App\Listeners\SendEmailToPartnerListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendEmailToAdminEvent::class => [
            SendEmailToAdminListener::class,
        ],
        SendEmailToPartnerEvent::class => [
            SendEmailToPartnerListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Painter::observe(new PainterObserver());
    }
}
